/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  guilh
 * Created: 17/07/2020
 */

create table tb_estado(
	id_estado serial NOT NULL UNIQUE,
	tx_nome_estado varchar(255) NOT NULL,
	tx_sigla_estado varchar(2) NOT NULL,
	CONSTRAINT pkidestado PRIMARY KEY (id_estado)
);

create table tb_cidade(
	id_cidade serial NOT NULL UNIQUE,
	tx_nome_cidade varchar(255) NOT NULL,
	id_estado integer NOT NULL,
	CONSTRAINT pkidcidade PRIMARY KEY (id_cidade),
	CONSTRAINT fkidestado FOREIGN KEY (id_estado) REFERENCES tb_estado (id_estado)
);

insert into tb_estado VALUES (1, 'Parana', 'PR');

insert into tb_cidade VALUES (1, 'Curitiba', 1);


create table tb_tipo_usuario(
	id_tipo_usuario serial NOT NULL UNIQUE,
	tx_tipo_usuario varchar(50) NOT NULL,
	CONSTRAINT pkidtipousuario PRIMARY KEY (id_tipo_usuario)
);

insert into tb_tipo_usuario VALUES (1, 'Juiz'), (2, 'Advogado'), (3, 'Parte');

create table tb_usuario(
	id_usuario serial NOT NULL UNIQUE,
	tx_nome_usuario varchar(255) NOT NULL,
	tx_login_usuario varchar(255) NOT NULL,
	tx_senha_usuario varchar(255) NOT NULL,
	tx_email_usuario varchar(255) NOT NULL,
	tx_cpf_usuario varchar(11) NOT NULL,
	tx_telefone_usuario varchar(30),
	id_cidade integer NOT NULL,
	id_tipo_usuario integer NOT NULL,
	id_advogado integer,
	CONSTRAINT pkidusuario PRIMARY KEY (id_usuario),
	CONSTRAINT uniquecpf UNIQUE (tx_cpf_usuario),
	CONSTRAINT fkidcidade FOREIGN KEY (id_cidade) REFERENCES tb_cidade (id_cidade),
	CONSTRAINT fkidtipousuario FOREIGN KEY (id_tipo_usuario) REFERENCES tb_tipo_usuario (id_tipo_usuario),
	CONSTRAINT fkidadvogado FOREIGN KEY (id_advogado) REFERENCES tb_usuario (id_usuario)
);

insert into tb_usuario VALUES (1, 'Guilherme', 'gui', md5('123'), 'guilherme@gmail.com', '11111111111', '41998564536', 1, 1, NULL);
insert into tb_usuario VALUES (2, 'Andre', 'andre', md5('234'), 'andre@gmail.com', '22222222222', '41998564536', 1, 2, NULL);
insert into tb_usuario VALUES (3, 'Gleidison', 'gleider', md5('345'), 'gleidison@gmail.com', '55555555555', '41998564536', 1, 2, NULL);
insert into tb_usuario VALUES (4, 'Carlos', 'carlos', md5('456'), 'carlos@gmail.com', '33333333333', '41998564536', 1, 3, 2);
insert into tb_usuario VALUES (5, 'Rafael', 'rafael', md5('567'), 'rafael@gmail.com', '44444444444', '41998564536', 1, 3, 3);

insert into tb_usuario VALUES (6, 'Razer', 'razer', md5('123'), 'razer@gmail.com', '66666666666', '41998564536', 1, 1, NULL);
insert into tb_usuario VALUES (7, 'Alex', 'alex', md5('234'), 'alex@gmail.com', '7777777777', '41998564536', 1, 2, NULL);
insert into tb_usuario VALUES (8, 'Luiz Antonio', 'luiz', md5('345'), 'luiz@gmail.com', '88888888888', '41998564536', 1, 2, NULL);
insert into tb_usuario VALUES (9, 'Rafaela', 'rafaela', md5('456'), 'rafaela@gmail.com', '99999999999', '41998564536', 1, 3, 7);
insert into tb_usuario VALUES (10, 'Jouglas', 'jouglas', md5('567'), 'jouglas@gmail.com', '10101010101', '41998564536', 1, 3, 8);

create table tb_tipo_fase(
	id_tipo_fase serial NOT NULL,
	tx_tipo_fase varchar(30) NOT NULL,
	CONSTRAINT pkidtipofase PRIMARY KEY (id_tipo_fase)
);

insert into tb_tipo_fase VALUES (1, 'Informativa'), (2, 'Deliberativa');

create table tb_processo(
	id_processo serial NOT NULL,
	tx_nome_processo varchar(255) NOT NULL,
	id_juiz integer NOT NULL,
	id_promovido integer NOT NULL,
	id_promovente integer NOT NULL,
	is_encerrado varchar(1) NOT NULL,
	id_vencedor_processo integer,
	CONSTRAINT pkidprocesso PRIMARY KEY (id_processo),
	CONSTRAINT fkidjuiz FOREIGN KEY (id_juiz) REFERENCES tb_usuario (id_usuario),
	CONSTRAINT fkidpromovido FOREIGN KEY (id_promovido) REFERENCES tb_usuario (id_usuario),
	CONSTRAINT fkidpromovente FOREIGN KEY (id_promovente) REFERENCES tb_usuario (id_usuario),
	CONSTRAINT fkvencedor FOREIGN KEY (id_vencedor_processo) REFERENCES tb_usuario (id_usuario)
);

insert into tb_processo VALUES 
(1, 'Teste' , 1, 4, 5, 'N');

create table tb_resposta_juiz(
	id_resposta_juiz serial NOT NULL,
	tx_resposta_juiz varchar(50) NOT NULL,
	CONSTRAINT pkidrespostajuiz PRIMARY KEY (id_resposta_juiz)
);

insert into tb_resposta_juiz VALUES 
(1, 'Aceito'),
(2, 'Negado'),
(3, 'Intimação'),
(4, 'Encerrado');

create table tb_fase_processo(
	id_fase_processo serial NOT NULL,
	id_processo integer NOT NULL,
	tx_titulo_fase varchar(255) NOT NULL,
	tx_descricao_fase varchar(255) NOT NULL,
	dt_criacao_fase timestamp NOT NULL,
	id_advogado_criador integer,
	arq_pdf varchar(500),
	id_tipo_fase integer NOT NULL,
	id_resposta_juiz integer,
	necessita_justificativa varchar(1),
	tx_justificativa varchar(500),
	dt_criacao_intimacao timestamp,
	tx_nome_intimado varchar(100),
	tx_cpf_intimado varchar(11),
	tx_endereco_intimado varchar(255),
	dt_execucao_intimacao timestamp,
	status_execucao_intimacao varchar(20),
	tx_parecer_final varchar(500),
	CONSTRAINT pkidfaseprocesso PRIMARY KEY (id_fase_processo),
	CONSTRAINT fkidprocesso FOREIGN KEY (id_processo) REFERENCES tb_processo (id_processo),
	CONSTRAINT fkadvogado FOREIGN KEY (id_advogado_criador) REFERENCES tb_usuario (id_usuario),
	CONSTRAINT fktipofase FOREIGN KEY (id_tipo_fase) REFERENCES tb_tipo_fase (id_tipo_fase),
	CONSTRAINT fkidrespostajuiz FOREIGN KEY (id_resposta_juiz) REFERENCES tb_resposta_juiz (id_resposta_juiz) 
);

create table tb_criterios_consulta(
	id_criterio_consulta serial NOT NULL,
	tx_criterio_consulta varchar(255) NOT NULL,
	CONSTRAINT pkidcriterioconsulta PRIMARY KEY (id_criterio_consulta)
);

insert into tb_criterios_consulta VALUES 
(1, 'Todos os processos'),
(2, 'Processos em aberto'),
(3, 'Processos já encerrados'),
(4, 'Processos em que foi promovido'),
(5, 'Processos em que foi promovente'),
(6, 'Processos que ganhou ou perdeu como promovido'),
(7, 'Processos que ganhou ou perdeu como promovente');

