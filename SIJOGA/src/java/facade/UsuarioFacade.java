/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import dao.UsuarioDAO;
import java.util.List;
import model.Cidade;
import model.Estado;
import model.TipoUsuario;
import model.Usuario;

/**
 *
 * @author guilh
 */
public class UsuarioFacade {
    private UsuarioDAO dao = new UsuarioDAO();
    
    public Usuario login(String login, String senha){        
        try{
            Usuario usr = dao.login(login, senha);
            return usr;
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }
    
    public List<Usuario> buscarJuizes(){
        try{
            List<Usuario> list = dao.buscarJuizes();
            if(!list.isEmpty()){
                return list;
            }else{
                throw new RuntimeException("Erro");
            }
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }
    
    public List<Usuario> buscarPartes(){
        try{
            List<Usuario> list = dao.buscarPartes();
            if(!list.isEmpty()){
                return list;
            }else{
                throw new RuntimeException("Erro");
            }
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }
    
    public void deslogar(Usuario usuarioLogado) {
        try{
            dao.deslogar(usuarioLogado);
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    public List<Usuario> buscarAdvogados() {
        try{
            List<Usuario> advogados = dao.buscarAdvogados();
            return advogados;
        }catch(Exception e){
            throw new RuntimeException("Erro ao buscar advogados");
        }
    }

    public List<Estado> getEstados() {
        try{
            List<Estado> estados = dao.getEstados();
            return estados;
        }catch(Exception e){
            throw new RuntimeException("Erro ao buscar estados");
        }
    }

    public List<Cidade> getCidades() {
        try{
            List<Cidade> cidades = dao.getCidades();
            return cidades;
        }catch(Exception e){
            throw new RuntimeException("Erro ao buscar cidades");
        }
    }

    public List<Cidade> getCidadesPorEstado(Estado estado) {
        try{
            List<Cidade> cidades = dao.getCidadesPorEstado(estado);
            return cidades;
        }catch(Exception e){
            throw new RuntimeException("Erro ao buscar cidades");
        }
    }
}
