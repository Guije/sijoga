/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import dao.ProcessoDAO;
import java.util.List;
import model.CriterioConsulta;
import model.FaseProcesso;
import model.Processo;
import model.RespostaJuiz;
import model.TipoFase;
import model.TipoUsuario;
import model.Usuario;

/**
 *
 * @author guilh
 */
public class ProcessoFacade {
    private ProcessoDAO dao = new ProcessoDAO();
    
    public boolean salvar(Processo p){
        try{
            boolean a = dao.salvar(p);
            if(a){
                return a;
            }else{
                throw new RuntimeException("Erro");
            }
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    public List<Processo> getProcessos(Usuario usr) {
        try{
            List<Processo> list = dao.getProcessos(usr);
            if(list != null){
                return list;
            }else{
                throw new RuntimeException("Erro");
            }
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }
    
    public TipoFase getTipoFase(String tipoFase){
        try{
            TipoFase tipo = dao.getTipoFase(tipoFase);
            if(tipo != null){
                return tipo;
            }else{
                throw new RuntimeException("Erro");
            }
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    public void salvarFaseProcesso(FaseProcesso faseProcesso) {
        try{
            dao.salvarFaseProcesso(faseProcesso);
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    public Processo getProcessoPorId(Long id) {
        try{
            Processo p = dao.buscarProcessoPorId(id);
            return p;
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    public List<RespostaJuiz> getRespostasJuiz() {
        try{
            List<RespostaJuiz> list = dao.getRespostasJuiz();
            return list;
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    public List<FaseProcesso> getFasesProcesso(Processo processoSelecionado) {
        try{
            List<FaseProcesso> list = dao.getFasesProcesso(processoSelecionado);
            return list;
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }
    
    public List<Processo> getProcessosComCriterio(Usuario usr, CriterioConsulta criterio){
        try{
            List<Processo> list = dao.getProcessosComCriterio(usr, criterio);
            return list;
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    public List<CriterioConsulta> getCriteriosConsulta() {
        try{
            List<CriterioConsulta> list = dao.getCriteriosConsulta();
            return list;
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    public List<Usuario> getUsuariosPorAdvogado(Usuario advogado) {
        try{
            List<Usuario> list = dao.getUsuariosPorAdvogado(advogado);
            return list;
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }
}
