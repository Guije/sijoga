/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import facade.ProcessoFacade;
import facade.UsuarioFacade;
import java.io.IOException;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import model.Processo;
import model.Usuario;

/**
 *
 * @author guilh
 */
@Named(value = "criaProcessoMBean")
@RequestScoped
public class CriaProcessoMBean {
    private Processo processo;
    private Usuario juiz;
    private Usuario promovido;
    private Usuario promovente;
    private List<Usuario> partes;
    private boolean consulta;

    public CriaProcessoMBean() {}
    
    @PostConstruct
    public void construir(){
        partes = getPartes();
        processo = new Processo();
        String id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");
        if(id != null && !id.equalsIgnoreCase("")){
            ProcessoFacade f = new ProcessoFacade();
            Long pId = Long.parseLong(id);
            setProcesso(f.getProcessoPorId(pId));
        }
        if(this.getProcesso() != null && this.getProcesso().getId() != null){
            consulta = true;
        }
    }
    
    public String salvar(){
        FacesContext context = FacesContext.getCurrentInstance();
        try{
            if(this.processo.getPromovente().getAdvogado().equals(this.processo.getPromovido().getAdvogado()) ){
                context.addMessage(null, new FacesMessage("Partes com o mesmo advogado!"));
            }
            ProcessoFacade f = new ProcessoFacade();
            processo.setEncerrado("N");
            boolean a = f.salvar(processo);
            if(a){
                FacesContext.getCurrentInstance().getExternalContext().redirect("painelControle.xhtml");
            }else{
                return "index.html";
            }
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }        
        return null;
    }

    public Usuario getJuiz() {
        return juiz;
    }

    public void setJuiz(Usuario juiz) {
        this.juiz = juiz;
    }

    public Usuario getPromovido() {
        return promovido;
    }

    public void setPromovido(Usuario promovido) {
        this.promovido = promovido;
    }

    public Usuario getPromovente() {
        return promovente;
    }

    public void setPromovente(Usuario promovente) {
        this.promovente = promovente;
    }

    public List<Usuario> getPartes() {
        UsuarioFacade f = new UsuarioFacade();
        partes = f.buscarPartes();
        return partes;
    }

    public void setPartes(List<Usuario> partes) {
        this.partes = partes;
    }

    public Processo getProcesso() {
        return processo;
    }

    public void setProcesso(Processo processo) {
        this.processo = processo;
    }

    public boolean isConsulta() {
        return consulta;
    }

    public void setConsulta(boolean consulta) {
        this.consulta = consulta;
    }
    
    public String voltar(){
        try{
            FacesContext.getCurrentInstance().getExternalContext().redirect("painelControle.xhtml");
        }catch(IOException e){
            throw new RuntimeException(e.getMessage());
        }
        return null;
    }
}
