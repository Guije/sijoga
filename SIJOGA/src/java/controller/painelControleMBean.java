/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import facade.ProcessoFacade;
import facade.UsuarioFacade;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import model.CriterioConsulta;
import model.FaseProcesso;
import model.Processo;
import model.RespostaJuiz;
import model.Usuario;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author guilh
 */
@Named(value = "painelControleMBean")
@ViewScoped
public class painelControleMBean implements Serializable {
    @Inject
    private LoginMBean usuarioLogado; //VER SE ESTA CORRETO
    private List<Processo> processos;
    private FaseProcesso faseProcesso = new FaseProcesso();
    private boolean informativa;
    private boolean deliberativa;
    private Processo processoSelecionado;
    private Long processoId;
    private RespostaJuiz respostaSelecionada = new RespostaJuiz();
    private List<RespostaJuiz> respostas;
    private ProcessoFacade processoFacade;
    private boolean consulta;
    private List<FaseProcesso> listaFases;
    private boolean permitirCriarFase;
    private List<CriterioConsulta> criterios;
    private CriterioConsulta criterioSelecionado;
    private byte[] arquivo;
    private FileUploadEvent event;
    private StreamedContent file;
    private String nomeImagem;
    
    public painelControleMBean() {}
    
    @PostConstruct
    public void init(){
        processos = getProcessos();
        criterios = getCriterios();
        if(processoSelecionado == null){
            processoSelecionado = new Processo();
        }
        consulta = false;
    }

    public Usuario getUsuarioLogado() {
        return usuarioLogado.getUsuarioLogado();
    }

    public void setUsuarioLogado(LoginMBean usuarioLogado) {
        this.usuarioLogado = usuarioLogado;
    }

    public List<Processo> getProcessos() {
        if(processos == null){
            ProcessoFacade p = new ProcessoFacade();
            processos = p.getProcessos(usuarioLogado.getUsuarioLogado().getUsuario());
        }
        return processos;
    }

    public void setProcessos(List<Processo> processos) {
        this.processos = processos;
    }
    
    public FaseProcesso getFaseProcesso() {
        return faseProcesso;
    }

    public void setFaseProcesso(FaseProcesso faseProcesso) {
        this.faseProcesso = faseProcesso;
    }

    public boolean isInformativa() {
        return informativa;
    }

    public void setInformativa(boolean informativa) {
        this.informativa = informativa;
    }

    public boolean isDeliberativa() {
        return deliberativa;
    }

    public void setDeliberativa(boolean deliberativa) {
        this.deliberativa = deliberativa;
    }

    public Processo getProcessoSelecionado() {
        return processoSelecionado;
    }

    public void setProcessoSelecionado(Processo processoSelecionado) {
        this.processoSelecionado = processoSelecionado;
    }

    public Long getProcessoId() {
        return processoId;
    }

    public void setProcessoId(Long processoId) {
        this.processoId = processoId;
    }

    public RespostaJuiz getRespostaSelecionada() {
        return respostaSelecionada;
    }

    public void setRespostaSelecionada(RespostaJuiz respostaSelecionada) {
        this.respostaSelecionada = respostaSelecionada;
    }

    public List<RespostaJuiz> getRespostas() {
        if(respostas == null){
            respostas = getProcessoFacade().getRespostasJuiz();
        }
        return respostas;
    }

    public void setRespostas(List<RespostaJuiz> respostas) {
        this.respostas = respostas;
    }

    public ProcessoFacade getProcessoFacade() {
        if(processoFacade == null){
            processoFacade = new ProcessoFacade();
        }
        return processoFacade;
    }

    public void setProcessoFacade(ProcessoFacade processoFacade) {
        this.processoFacade = processoFacade;
    }

    public boolean isConsulta() {
        return consulta;
    }

    public void setConsulta(boolean consulta) {
        this.consulta = consulta;
    }

    public List<FaseProcesso> getListaFases() {
        if(listaFases == null){
            listaFases = new ArrayList<FaseProcesso>();
        }
        
        if(getProcessoSelecionado() != null && getProcessoSelecionado().getId() != null){
            listaFases = getProcessoFacade().getFasesProcesso(processoSelecionado);
        }
        return listaFases;
    }

    public void setListaFases(List<FaseProcesso> listaFases) {
        this.listaFases = listaFases;
    }

    public boolean isPermitirCriarFase() {
        return permitirCriarFase;
    }

    public void setPermitirCriarFase(boolean permitirCriarFase) {
        this.permitirCriarFase = permitirCriarFase;
    }

    public List<CriterioConsulta> getCriterios() {
        if(criterios == null){
            criterios = getProcessoFacade().getCriteriosConsulta();
        }
        return criterios;
    }

    public void setCriterios(List<CriterioConsulta> criterios) {
        this.criterios = criterios;
    }

    public CriterioConsulta getCriterioSelecionado() {
        return criterioSelecionado;
    }

    public void setCriterioSelecionado(CriterioConsulta criterioSelecionado) {
        this.criterioSelecionado = criterioSelecionado;
    }

    public byte[] getArquivo() {
        return arquivo;
    }

    public void setArquivo(byte[] arquivo) {
        this.arquivo = arquivo;
    }

    public FileUploadEvent getEvent() {
        return event;
    }

    public void setEvent(FileUploadEvent event) {
        this.event = event;
    }

    public StreamedContent getFile() throws FileNotFoundException, IOException {
        InputStream stream = new FileInputStream(this.faseProcesso.getDiretorioImagem());
        file = new DefaultStreamedContent(stream, "application/pdf", salvarImagem(this.event.getFile().getContentType()));
        return file;
    }

    public void setFile(StreamedContent file) {
        this.file = file;
    }

    public String getNomeImagem() {
        return nomeImagem;
    }

    public void setNomeImagem(String nomeImagem) {
        this.nomeImagem = nomeImagem;
    }
       
    public String criarProcesso(){
        return "criaProcesso";
    }
   
    public void salvarFase() throws IOException{
        FacesContext context = FacesContext.getCurrentInstance();
        
        if(getProcessoSelecionado() != null && getProcessoSelecionado().getId() != null){
            if(this.necessitaJustificativa()){
                if(faseProcesso.getJustificativa() == null || faseProcesso.getJustificativa().isEmpty()){
                    context.addMessage(null, new FacesMessage("Justificativa necessária"));
                    return;
                }
            }
        
            faseProcesso.setData(new Timestamp(System.currentTimeMillis()));

            if(informativa){
                faseProcesso.setTipoFase(getProcessoFacade().getTipoFase("Informativa"));
            }else{
                faseProcesso.setTipoFase(getProcessoFacade().getTipoFase("Deliberativa"));
            }

            faseProcesso.setAdvogadoCriador(getUsuarioLogado());

            faseProcesso.setProcesso(getProcessoSelecionado());
            
            this.arquivo = event.getFile().getContents();
            this.faseProcesso.setArq((salvarImagem(this.event.getFile().getContentType())));   
            
			
            try{
                getProcessoFacade().salvarFaseProcesso(faseProcesso);
                setListaFases(getListaFases());
                limpar();
                context.addMessage(null, new FacesMessage("Fase cadastrada com sucesso"));
            }catch(Exception e){
                context.addMessage(null, new FacesMessage(e.getMessage()));
            } 
        }
    }
    
    public void limpar(){
        consulta = false;
        this.faseProcesso = new FaseProcesso();
        this.informativa = false;
        this.deliberativa = false;
    }
    
    public void salvarRespostaJuiz(){
        FacesContext context = FacesContext.getCurrentInstance();
        
        if (("Aceito").equalsIgnoreCase(respostaSelecionada.getNome())){
           faseProcesso.setResposta(respostaSelecionada);
        } else if (("Negado").equalsIgnoreCase(respostaSelecionada.getNome())) {
            faseProcesso.setResposta(respostaSelecionada);
            faseProcesso.setNecessitaJustificativa("Y");
        } else if (("Intimação").equalsIgnoreCase(respostaSelecionada.getNome())) {
            //FUTURA IMPLEMENTÇÃO
        } else if (("Encerrado").equalsIgnoreCase(respostaSelecionada.getNome())) {
            faseProcesso.setResposta(respostaSelecionada);
            processoSelecionado.setEncerrado("Y");
        } 
        try{
            getProcessoFacade().salvarFaseProcesso(faseProcesso);
            context.addMessage(null, new FacesMessage("Parecer concluído!"));
        }catch(Exception e){
            context.addMessage(null, new FacesMessage(e.getMessage()));
        }
    }
    
    public void consultarFase(){
        consulta = true;
        if(faseProcesso != null){
            if(("Informativa").equalsIgnoreCase(this.faseProcesso.getTipoFase().getNome())){
                informativa = true;
            }else{
                deliberativa = true;
            }
        }  
    }
    
    public void criarNovaFase(){
        consulta = false;
        this.faseProcesso = new FaseProcesso();
    }
    
    public void darParecer(){
        consulta = false;
    }
    
    public void permitirCriarFase(){
        if(processoSelecionado != null && processoSelecionado.getId() != null){
            if(("Advogado").equalsIgnoreCase(getUsuarioLogado().getTipoUsuario().getNome())
                && ("N").equalsIgnoreCase(processoSelecionado.getEncerrado())){
                if(!getListaFases().isEmpty()){
                    if(("Informativa").equalsIgnoreCase(getListaFases().get(getListaFases().size() - 1).getTipoFase().getNome())){
                        permitirCriarFase = true;
                    }else{
                        if(getListaFases().get(getListaFases().size() - 1).getResposta() == null){
                            permitirCriarFase = false;
                        }else{
                            permitirCriarFase = true;
                        }
                    }
                }else{
                    permitirCriarFase = true;
                }
            }else{
                permitirCriarFase = false;
            }
        }else{
            permitirCriarFase = false;
        }
    }
    
    public void abrirDialogFases(){
        setListaFases(getListaFases());
        permitirCriarFase();
        consulta = false;
    }
    
    public boolean necessitaJustificativa(){
        if(processoSelecionado != null && processoSelecionado.getId() != null && !getListaFases().isEmpty()){
            for(FaseProcesso obj: getListaFases()){
                if(("Y").equalsIgnoreCase(obj.getNecessitaJustificativa())){
                    return true;
                }
            }
        }
        return false;
    }
    
    public List<Usuario> buscarPartes(){
        if(faseProcesso != null && faseProcesso.getId() != null && respostaSelecionada != null){
            if(("Encerrado").equalsIgnoreCase(respostaSelecionada.getNome())){
                List<Usuario> list = new ArrayList<Usuario>();
                if(processoSelecionado != null && processoSelecionado.getId() != null && !getListaFases().isEmpty()){
                    list.add(processoSelecionado.getPromovente());
                    list.add(processoSelecionado.getPromovido());
                }
        
                return list;
            }
        }        
        return null;
    }
    
    public boolean isAguardando(Processo proc){
        if(proc != null && proc.getId() != null && !("Parte").equalsIgnoreCase(getUsuarioLogado().getTipoUsuario().getNome())){
            processoSelecionado = proc;
            if(getListaFases().size() > 1){
                if(("Deliberativa").equalsIgnoreCase(getListaFases().get(getListaFases().size() - 1).getTipoFase().getNome())
                    && (getListaFases().get(getListaFases().size() - 1).getResposta() == null)){
                    return true;
                }
            }
        }
        return false;
    }
    
    public List<Processo> buscarProcessos(){
        if(criterioSelecionado != null && criterioSelecionado.getId() != null){
            processos = getProcessoFacade().getProcessosComCriterio(getUsuarioLogado(), criterioSelecionado);
        }
        return processos;
    } 
    
    public String consultarProcesso(){
        if(processoSelecionado != null && processoSelecionado.getId() != null){
            try{
                FacesContext.getCurrentInstance().getExternalContext().redirect("criaProcesso.xhtml?id="+processoSelecionado.getId());
            }catch(IOException e){
                throw new RuntimeException(e.getMessage());
            }
        }else{
            return null;
        }
        return null;
    }
    
    public void handleFileUpload(FileUploadEvent event) throws IOException {
        this.event = event;

        //COLOCAR MSG DE AVISO
    }
    
    private String salvarImagem(String tipoArquivo) throws IOException{
        String extensao[] = tipoArquivo.split("/");

        this.nomeImagem = this.faseProcesso.getTitulo() + "." + extensao[1];

        File file = new File(this.faseProcesso.getDiretorioImagem() + nomeImagem);

        FileOutputStream out = new FileOutputStream(file);
        out.write(arquivo);
        out.close();

        return this.nomeImagem;
    }
    
    public StreamedContent getImagemPdf() throws Exception {
        String imageName = FacesContext.getCurrentInstance()
            .getExternalContext().getRequestParameterMap().get("imagePdf");

        if (imageName != null) {
            File temp = new File(imageName);
            if (temp.exists()) {
                return new DefaultStreamedContent(new FileInputStream(temp));
            }
        }

        return new DefaultStreamedContent();
    }
    
    public void excluirArq(String url){
        if(event != null){
            event = null;
        }
    }
    
    public void baixarArq(FaseProcesso fase){
        this.faseProcesso = fase;
    }
    
    public void deslogar(){
        try{
            UsuarioFacade f = new UsuarioFacade();
            f.deslogar(getUsuarioLogado());
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }   
    }
}
