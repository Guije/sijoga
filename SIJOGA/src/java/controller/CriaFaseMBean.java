/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import facade.ProcessoFacade;
import java.sql.Date;
import java.sql.Timestamp;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import model.FaseProcesso;
import model.Processo;
import model.Usuario;

/**
 *
 * @author guilh
 */
@Named(value = "criaFaseMBean")
@RequestScoped
public class CriaFaseMBean {
    @Inject
    private LoginMBean usuarioLogado;
    private FaseProcesso faseProcesso = new FaseProcesso();
    private boolean informativa;
    private boolean deliberativa;
    private Processo processo;
    
    public CriaFaseMBean() {}
    
    public void init(){
        String id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");
        if(id != null && !id.equalsIgnoreCase("")){
            Long pId = Long.parseLong(id);
            setProcesso(buscarProcesso(pId));
        }
    }
        
    
    /*public String salvarFase(){
        ProcessoFacade f = new ProcessoFacade();
        
        faseProcesso.setData(new Timestamp(System.currentTimeMillis()));
        
        if(informativa){
            faseProcesso.setTipoFase(f.getTipoFase("Informativa"));
        }else{
            faseProcesso.setTipoFase(f.getTipoFase("Deliberativa"));
        }
        
        faseProcesso.setAdvogadoCriador(getUsuarioLogado());
        
        faseProcesso.setProcesso(processo);
        
        f.salvarFaseProcesso(faseProcesso);
        
        return "painelControle";
    }*/

    public Usuario getUsuarioLogado() {
        return usuarioLogado.getUsuarioLogado();
    }

    public void setUsuarioLogado(LoginMBean usuarioLogado) {
        this.usuarioLogado = usuarioLogado;
    }

    public FaseProcesso getFaseProcesso() {
        return faseProcesso;
    }

    public void setFaseProcesso(FaseProcesso faseProcesso) {
        this.faseProcesso = faseProcesso;
    }

    public boolean isInformativa() {
        return informativa;
    }

    public void setInformativa(boolean informativa) {
        this.informativa = informativa;
    }

    public boolean isDeliberativa() {
        return deliberativa;
    }

    public void setDeliberativa(boolean deliberativa) {
        this.deliberativa = deliberativa;
    }

    public Processo getProcesso() {
        return processo;
    }

    public void setProcesso(Processo processo) {
        this.processo = processo;
    }
    
    public Processo buscarProcesso(Long id){
        if(processo == null){
            ProcessoFacade f = new ProcessoFacade();
            processo = f.getProcessoPorId(id);
        }
        return processo;
    }
}
