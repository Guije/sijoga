/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import facade.UsuarioFacade;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import model.Cidade;
import model.Estado;
import model.TipoUsuario;
import model.Usuario;

/**
 *
 * @author guilh
 */
@Named(value = "criarUsuarioMBean")
@RequestScoped
public class criarUsuarioMBean {
    private Usuario usuario = new Usuario();
    private List<Usuario> advogados;
    private List<Estado> estados;
    private List<Cidade> cidades;
    private UsuarioFacade facade;
    private Estado estado;
    private boolean liberar = false;
    
    public criarUsuarioMBean() {}
    
    public void init(){
        usuario = new Usuario();
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public List<Usuario> getAdvogados() {
        if(advogados == null || advogados.isEmpty()){
            advogados = getFacade().buscarAdvogados();
        }
        return advogados;
    }

    public void setAdvogados(List<Usuario> advogados) {
        this.advogados = advogados;
    }

    public List<Estado> getEstados() {
        if(estados == null || estados.isEmpty()){
            estados = getFacade().getEstados();
        }
        return estados;
    }

    public void setEstados(List<Estado> estados) {
        this.estados = estados;
    }

    public List<Cidade> getCidades() {
        if(cidades == null || cidades.isEmpty()){
            cidades = getFacade().getCidades();
        }
        
        return cidades;
    }

    public void setCidades(List<Cidade> cidades) {
        this.cidades = cidades;
    }

    public UsuarioFacade getFacade() {
        if(facade == null){
            facade = new UsuarioFacade();
        }
        return facade;
    }

    public void setFacade(UsuarioFacade facade) {
        this.facade = facade;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public boolean isLiberar() {
        return liberar;
    }

    public void setLiberar(boolean liberar) {
        this.liberar = liberar;
    }

    public void atualizarCidades(){
        if(estado != null && estado.getId() != null){
            cidades = getFacade().getCidadesPorEstado(estado);
            liberar = true;
        }
    }
}
