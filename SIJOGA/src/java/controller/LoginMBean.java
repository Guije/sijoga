/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import facade.UsuarioFacade;
import java.io.IOException;
import java.io.Serializable;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import model.Usuario;
import java.security.*;
import java.math.*;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author guilh
 */
@Named(value = "loginMBean")
@SessionScoped
public class LoginMBean implements Serializable{
    private String login;
    private String senha;
    private Usuario usuarioLogado;

    public LoginMBean() {}
    
    public String logar() throws IOException{
        FacesContext context = FacesContext.getCurrentInstance();
        if(getLogin() == null || getSenha() == null){
            context.addMessage(null, new FacesMessage("Login/Senha inválido"));
        }
        
        try{
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.update(senha.getBytes(),0,senha.length());
            byte[] digest = m.digest();
            StringBuffer sb = new StringBuffer();

            for (byte b : digest)
                sb.append(String.format("%02x", b & 0xff));
            senha = sb.toString();
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
        
        UsuarioFacade logFac = new UsuarioFacade();
        try{
            usuarioLogado = logFac.login(this.getLogin(), this.getSenha());
        }catch(Exception e){
            context.addMessage(null, new FacesMessage(e.getMessage()));
            return "index";
        }
        
        if(usuarioLogado != null){
            FacesContext.getCurrentInstance().getExternalContext().redirect("painelControle.xhtml");
        }else{
            context.addMessage(null, new FacesMessage("Usuario não encontrado"));
            return "index";
        }
        return "index";
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    public Usuario getUsuarioLogado(){
        return this.usuarioLogado;
    }
}
