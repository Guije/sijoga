/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.component.html.HtmlDataTable;

/**
 *
 * @author guilh
 */
@Named(value = "manipuladorDataTableMBean")
@Dependent
public class ManipuladorDataTableMBean {

    private static final long serialVersionUID = 1L;
         
    private HtmlDataTable dataTable;
  
    public HtmlDataTable getDataTable() {
          return dataTable;
    }

    public void setDataTable(HtmlDataTable dataTable) {
          this.dataTable = dataTable;
    }     
}
