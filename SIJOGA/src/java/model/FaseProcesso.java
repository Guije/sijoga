/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.faces.context.FacesContext;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author guilh
 */
@Entity
@Table(name="tb_fase_processo")
@SequenceGenerator(name = "sequencia_fase_processo", sequenceName = "tb_fase_processo_id_fase_processo_seq" )
public class FaseProcesso implements Serializable {
    private Long id;
    private Processo processo;
    private String titulo;
    private String descricao;
    private Timestamp data;
    private Usuario advogadoCriador;
    private String arq;
    private TipoFase tipoFase;
    private RespostaJuiz resposta;
    private String necessitaJustificativa;
    private String justificativa;
    private Timestamp dataCriacaoIntimacao;
    private String nomeIntimado;
    private String cpfIntimado;
    private String enderecoIntimado;
    private Timestamp dataExecucao;
    private String statusExecucao;
    private String parecer;
    private @Transient String urlArq;

    public FaseProcesso() {}

    @Id
    @Column(updatable = false, name = "id_fase_processo", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequencia_fase_processo")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FaseProcesso)) {
            return false;
        }
        FaseProcesso other = (FaseProcesso) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.FaseProcesso[ id=" + id + " ]";
    }

    @ManyToOne
    @JoinColumn(name = "id_processo")
    public Processo getProcesso() {
        return processo;
    }

    public void setProcesso(Processo processo) {
        this.processo = processo;
    }

    @Column(name = "tx_titulo_fase")
    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    @Column(name = "tx_descricao_fase")
    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Column(name = "dt_criacao_fase")
    public Timestamp getData() {
        return data;
    }

    public void setData(Timestamp data) {
        this.data = data;
    }

    @ManyToOne
    @JoinColumn(name = "id_advogado_criador", updatable = true)
    public Usuario getAdvogadoCriador() {
        return advogadoCriador;
    }

    public void setAdvogadoCriador(Usuario advogadoCriador) {
        this.advogadoCriador = advogadoCriador;
    }

    @Column(name = "arq_pdf")
    public String getArq() {
        return arq;
    }

    public void setArq(String arq) {
        this.arq = arq;
    }

    @ManyToOne
    @JoinColumn(name = "id_tipo_fase")
    public TipoFase getTipoFase() {
        return tipoFase;
    }

    public void setTipoFase(TipoFase tipoFase) {
        this.tipoFase = tipoFase;
    }

    @ManyToOne
    @JoinColumn(name = "id_resposta_juiz")
    public RespostaJuiz getResposta() {
        return resposta;
    }

    public void setResposta(RespostaJuiz resposta) {
        this.resposta = resposta;
    }
    
    @Column(name = "necessita_justificativa")
    public String getNecessitaJustificativa() {
        return necessitaJustificativa;
    }

    public void setNecessitaJustificativa(String necessitaJustificativa) {
        this.necessitaJustificativa = necessitaJustificativa;
    }
    
    @Column(name = "tx_justificativa")
    public String getJustificativa() {
        return justificativa;
    }

    public void setJustificativa(String justificativa) {
        this.justificativa = justificativa;
    }

    @Column(name = "dt_criacao_intimacao")
    public Timestamp getDataCriacaoIntimacao() {
        return dataCriacaoIntimacao;
    }

    public void setDataCriacaoIntimacao(Timestamp dataCriacaoIntimacao) {
        this.dataCriacaoIntimacao = dataCriacaoIntimacao;
    }

    @Column(name = "tx_nome_intimado")
    public String getNomeIntimado() {
        return nomeIntimado;
    }

    public void setNomeIntimado(String nomeIntimado) {
        this.nomeIntimado = nomeIntimado;
    }

    @Column(name = "tx_cpf_intimado")
    public String getCpfIntimado() {
        return cpfIntimado;
    }

    public void setCpfIntimado(String cpfIntimado) {
        this.cpfIntimado = cpfIntimado;
    }

    @Column(name = "tx_endereco_intimado")
    public String getEnderecoIntimado() {
        return enderecoIntimado;
    }

    public void setEnderecoIntimado(String enderecoIntimado) {
        this.enderecoIntimado = enderecoIntimado;
    }

    @Column(name = "dt_execucao_intimacao")
    public Timestamp getDataExecucao() {
        return dataExecucao;
    }

    public void setDataExecucao(Timestamp dataExecucao) {
        this.dataExecucao = dataExecucao;
    }

    @Column(name = "status_execucao_intimacao")
    public String getStatusExecucao() {
        return statusExecucao;
    }

    public void setStatusExecucao(String statusExecucao) {
        this.statusExecucao = statusExecucao;
    }

    @Column(name = "tx_parecer_final")
    public String getParecer() {
        return parecer;
    }

    public void setParecer(String parecer) {
        this.parecer = parecer;
    }
    
    public boolean necessarioParecer(Usuario usuarioLogado){
        if(("Y").equalsIgnoreCase(this.processo.getEncerrado())){
            return false;
        }
        if(this.resposta == null){
            return ("Juiz").equalsIgnoreCase(usuarioLogado.getTipoUsuario().getNome());
        }else{
            return false;
        }        
    }
    
    @Transient
    public String getStatusFase(){
        if(this.resposta != null){
            if(this.resposta.getNome() != null){
                return this.resposta.getNome();
            }else{
                if(("Deliberativa").equalsIgnoreCase(this.getTipoFase().getNome())){
                    return "Aguardando parecer";
                }else{
                    return "Parecer não necessário";
                }
            } 
        }else{
            if(("Y").equalsIgnoreCase(this.processo.getEncerrado())){
                return "Parecer não feito";
            }
            if(("Deliberativa").equalsIgnoreCase(this.getTipoFase().getNome())){
                return "Aguardando parecer";
            }else{
                return "Parecer não necessário";
            }
        } 
    }
    
    public @Transient String getDiretorioImagem(){
        return "C:\\arquivos_sijoga\\";
    }
    
    public @Transient String getUrlArq(){
        return this.urlArq = getArq() != null ? getDiretorioImagem() + getArq() : null;
    }

    public void setUrlArq(String urlArq) {
        this.urlArq = urlArq;
    }
}
