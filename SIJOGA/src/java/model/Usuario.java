/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import facade.ProcessoFacade;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import util.SampleEntity;

/**
 *
 * @author guilh
 */
@Entity
@Table(name="tb_usuario")
@SequenceGenerator(name = "sequencia_usuario", sequenceName = "tb_usuario_id_usuario_seq" )
public class Usuario implements Serializable, SampleEntity {
    private Long id;
    private String nome;
    private String login;
    private String senha;
    private String email;
    private String cpf;
    private String telefone;
    private Cidade cidade;
    private TipoUsuario tipoUsuario;
    private Usuario advogado;
    private @Transient Collection<FaseProcesso> faseProcesso;
    private @Transient List<Processo> processosJuiz;
    private @Transient List<Processo> processosPromoventes;
    private @Transient List<Processo> processosPromovidos;
    private @Transient List<FaseProcesso> fasesVencidas;
    private @Transient String concatParteAdvogado;
    
    @Id
    @Column(insertable = false, updatable = false, name = "id_usuario", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequencia_usuario")
    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if (this.id != null || other.id == null && (this.id == null || this.id.equals(other.id))) {
        } else {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Usuario[ id=" + id + " ]";
    }

    @Column(name = "tx_nome_usuario")
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Column(name = "tx_login_usuario")
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Column(name = "tx_senha_usuario")
    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    @Column(name = "tx_email_usuario")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "tx_cpf_usuario")
    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    @Column(name = "tx_telefone_usuario")
    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    @ManyToOne
    @JoinColumn(name = "id_cidade")
    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    @ManyToOne
    @JoinColumn(name = "id_tipo_usuario")
    public TipoUsuario getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(TipoUsuario tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    @OneToOne(mappedBy = "usuario")
    public @Transient Usuario getUsuario(){
        return this;
    }
    
    @ManyToOne
    @JoinColumn(name = "id_advogado", updatable = true)
    public Usuario getAdvogado() {
        return advogado;
    }

    public void setAdvogado(Usuario advogado) {
        this.advogado = advogado;
    }

    @OneToMany(mappedBy = "advogadoCriador", cascade = CascadeType.ALL)
    public @Transient Collection<FaseProcesso> getFaseProcesso() {
        return faseProcesso;
    }

    public void setFaseProcesso(Collection<FaseProcesso> faseProcesso) {
        this.faseProcesso = faseProcesso;
    }
    
    @OneToMany(mappedBy = "juiz", cascade = CascadeType.ALL)
    public @Transient List<Processo> getProcessosJuiz() {
        return processosJuiz;
    }

    public void setProcessosJuiz(List<Processo> processosJuiz) {
        this.processosJuiz = processosJuiz;
    }

    @OneToMany(mappedBy = "promovente", cascade = CascadeType.ALL)
    public @Transient List<Processo> getProcessosPromoventes() {
        return processosPromoventes;
    }

    public void setProcessosPromoventes(List<Processo> processosPromoventes) {
        this.processosPromoventes = processosPromoventes;
    }

    @OneToMany(mappedBy = "promovente", cascade = CascadeType.ALL)
    public @Transient List<Processo> getProcessosPromovidos() {
        return processosPromovidos;
    }

    public void setProcessosPromovidos(List<Processo> processosPromovidos) {
        this.processosPromovidos = processosPromovidos;
    }

    @OneToMany(mappedBy = "vencedor", cascade = CascadeType.ALL)
    public @Transient List<FaseProcesso> getFasesVencidas() {
        return fasesVencidas;
    }

    public void setFasesVencidas(List<FaseProcesso> fasesVencidas) {
        this.fasesVencidas = fasesVencidas;
    }
    
    public @Transient boolean isUsuarioJuiz(){
        if(this.getTipoUsuario().getNome().equalsIgnoreCase("Juiz")){
            return true;
        }else{
            return false;
        }   
    }
    
    public @Transient boolean isUsuarioAdvogado(){
        if(this.getTipoUsuario().getNome().equalsIgnoreCase("Advogado")){
            return true;
        }else{
            return false;
        }
    }
    
    public @Transient boolean isUsuarioParte(){
        if(this.getTipoUsuario().getNome().equalsIgnoreCase("Parte")){
            return true;
        }else{
            return false;
        }
    }
    
    @Transient
    public Integer getQuantidadeProcessos() {
        ProcessoFacade f = new ProcessoFacade();        
        return f.getProcessos(this).size();
    }
    
    public @Transient String getConcatParteAdvogado(){
        if(("Parte").equalsIgnoreCase(this.tipoUsuario.getNome())){
            this.concatParteAdvogado = this.nome + " - Advogado: " + this.advogado.nome;
        }else{
            this.concatParteAdvogado = "";
        }
        return this.concatParteAdvogado;
    }
}
