/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import util.SampleEntity;

/**
 *
 * @author guilh
 */
@Entity
@Table(name="tb_criterios_consulta")
@SequenceGenerator(name = "sequencia_criterio_consulta", sequenceName = "tb_criterios_consulta_id_criterio_consulta_seq" )
public class CriterioConsulta implements Serializable, SampleEntity {
    private Long id;
    private String nome;

    @Id
    @Column(updatable = false, name = "id_criterio_consulta", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequencia_criterio_consulta")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CriterioConsulta)) {
            return false;
        }
        CriterioConsulta other = (CriterioConsulta) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.CriterioConsulta[ id=" + id + " ]";
    }

    @Column(name = "tx_criterio_consulta")
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
