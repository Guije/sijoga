/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author guilh
 */
@Entity
@Table(name="tb_processo")
@SequenceGenerator(name = "sequencia_processo", sequenceName = "tb_processo_id_processo_seq" )
public class Processo implements Serializable {
    private Long id;
    private String nome;
    private Usuario juiz;
    private Usuario promovido;
    private Usuario promovente;
    private String encerrado;
    private Usuario vencedor;
    private @Transient Collection<TipoFase> tipoFase;
    private @Transient Collection<FaseProcesso> faseProcesso;
    
    @Id
    @Column(updatable = false, name = "id_processo", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequencia_processo")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Processo)) {
            return false;
        }
        Processo other = (Processo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Processo[ id=" + id + " ]";
    }

    @Column(name = "tx_nome_processo")
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @ManyToOne
    @JoinColumn(name = "id_juiz")
    public Usuario getJuiz() {
        return juiz;
    }

    public void setJuiz(Usuario juiz) {
        this.juiz = juiz;
    }

    @ManyToOne
    @JoinColumn(name = "id_promovido")
    public Usuario getPromovido() {
        return promovido;
    }

    public void setPromovido(Usuario promovido) {
        this.promovido = promovido;
    }

    @ManyToOne
    @JoinColumn(name = "id_promovente")
    public Usuario getPromovente() {
        return promovente;
    }

    public void setPromovente(Usuario promovente) {
        this.promovente = promovente;
    }

    @Column(name = "is_encerrado", length = 1)
    public String getEncerrado() {
        return encerrado;
    }

    public void setEncerrado(String encerrado) {
        this.encerrado = encerrado;
    }

    @ManyToMany(targetEntity = model.TipoFase.class, cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinTable(name = "tb_fase_processo", joinColumns = {@JoinColumn(name = "id_processo")}, inverseJoinColumns = {@JoinColumn(name="id_tipo_fase")})
    public @Transient Collection<TipoFase> getTipoFase() {
        return tipoFase;
    }

    public void setTipoFase(Collection<TipoFase> tipoFase) {
        this.tipoFase = tipoFase;
    }

    @OneToMany(mappedBy = "processo", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    public @Transient Collection<FaseProcesso> getFaseProcesso() {
        return faseProcesso;
    }

    public void setFaseProcesso(Collection<FaseProcesso> faseProcesso) {
        this.faseProcesso = faseProcesso;
    }
    
    @ManyToOne
    @JoinColumn(name = "id_vencedor_processo")
    public Usuario getVencedor() {
        return vencedor;
    }

    public void setVencedor(Usuario vencedor) {
        this.vencedor = vencedor;
    }
    
    public String qualParte(Long idAdvogado){
        if(idAdvogado != null){
            if(getPromovente().getId().equals(idAdvogado)){
                return "Promovente";
            }else{
                return "Promovido";
            }
        }
        return "";
    }
}
