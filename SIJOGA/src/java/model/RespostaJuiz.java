/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import util.SampleEntity;

/**
 *
 * @author guilh
 */
@Entity
@Table(name="tb_resposta_juiz")
@SequenceGenerator(name = "sequencia_resposta_juiz", sequenceName = "tb_respota_juiz_id_resposta_juiz_seq" )
public class RespostaJuiz implements Serializable, SampleEntity {
    private Long id;
    private String nome;
    private @Transient Collection<FaseProcesso> fases;

    @Id
    @Column(updatable = false, name = "id_resposta_juiz", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequencia_resposta_juiz")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RespostaJuiz)) {
            return false;
        }
        RespostaJuiz other = (RespostaJuiz) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.RespostaJuiz[ id=" + id + " ]";
    }

    @Column(name = "tx_resposta_juiz")
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @OneToMany(mappedBy = "resposta", cascade = CascadeType.ALL)
    public @Transient Collection<FaseProcesso> getFases() {
        return fases;
    }

    public void setFases(Collection<FaseProcesso> fases) {
        this.fases = fases;
    }
}
