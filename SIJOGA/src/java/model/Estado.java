/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import util.SampleEntity;

/**
 *
 * @author guilh
 */
@Entity
@Table(name="tb_estado")
@SequenceGenerator(name = "sequencia_estado", sequenceName = "tb_estado_id_estado_seq" )
public class Estado implements Serializable, SampleEntity {
    private Long id;
    private String nome;
    private String sigla; 
    private @Transient Collection<Cidade> cidade;

    @Id
    @Column(updatable = false, name = "id_estado", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequencia_estado")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Estado)) {
            return false;
        }
        Estado other = (Estado) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Estado[ id=" + id + " ]";
    }
    
    @Column(name = "tx_nome_estado")
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Column(name="tx_sigla_estado")
    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    @OneToMany(mappedBy = "estado")
    public Collection<Cidade> getCidade() {
        return cidade;
    }

    public void setCidade(Collection<Cidade> cidade) {
        this.cidade = cidade;
    }
}
