/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author guilh
 */
@Entity
@Table(name="tb_tipo_fase")
@SequenceGenerator(name = "sequencia_tipo_fase", sequenceName = "tb_tipo_fase_id_tipo_fase_seq" )
public class TipoFase implements Serializable {
    private Long id;
    private String nome;
    private @Transient Collection<Processo> processos;
    private @Transient Collection<FaseProcesso> faseProcesso;

    @Id
    @Column(updatable = false, name = "id_tipo_fase", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequencia_tipo_fase")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoFase)) {
            return false;
        }
        TipoFase other = (TipoFase) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.TipoProcesso[ id=" + id + " ]";
    }

    @Column(name = "tx_tipo_fase")
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @ManyToMany(cascade = {CascadeType.PERSIST,CascadeType.MERGE}, mappedBy = "tipoFase", fetch = FetchType.EAGER)
    public @Transient Collection<Processo> getProcessos() {
        return processos;
    }

    public void setProcessos(Collection<Processo> processos) {
        this.processos = processos;
    }

    @OneToMany(mappedBy = "tipoFase", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    public @Transient Collection<FaseProcesso> getFaseProcesso() {
        return faseProcesso;
    }

    public void setFaseProcesso(Collection<FaseProcesso> faseProcesso) {
        this.faseProcesso = faseProcesso;
    }
}
