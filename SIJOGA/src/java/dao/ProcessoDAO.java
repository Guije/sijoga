/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import facade.UsuarioFacade;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import model.CriterioConsulta;
import model.FaseProcesso;
import model.Processo;
import model.RespostaJuiz;
import model.TipoFase;
import model.TipoUsuario;
import model.Usuario;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import util.HibernateUtil;

/**
 *
 * @author guilh
 */
public class ProcessoDAO {
    
    public boolean salvar(Processo p){
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            
            session.beginTransaction();
            
            p.setJuiz(this.getJuizComMenosProcessos());
            
            session.save(p);
            session.getTransaction().commit();
            
            session.close();
            
            return true;
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    public List<Processo> getProcessos(Usuario usr) {
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            
            Query query;
            
            if(("Juiz").equalsIgnoreCase(usr.getTipoUsuario().getNome())){
                query = session.createQuery(
                "FROM Processo p " +
                "WHERE p.juiz.id = :id");
                query.setInteger("id", usr.getId().intValue());
            }else if(("Advogado").equalsIgnoreCase(usr.getTipoUsuario().getNome())){
                query = session.createQuery(
                "FROM Processo p " +
                "WHERE p.promovente.advogado.id = :id OR p.promovido.advogado.id = :id");
                query.setInteger("id", usr.getId().intValue());
            }else{
                 query = session.createQuery(
                "FROM Processo p " +
                "WHERE p.promovente.id = :id OR p.promovido.id = :id");
                query.setInteger("id", usr.getId().intValue());
            }

            List<Processo> list = query.list();
            
            session.close();
            
            return list;
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }
    
    public List<Processo> getProcessosComCriterio(Usuario usr, CriterioConsulta criterio) {
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            
            Query query;
            
            if(("Processos em aberto").equalsIgnoreCase(criterio.getNome())){
                query = session.createQuery(
                "FROM Processo p " +
                "WHERE p.promovente.advogado.id = :id OR p.promovido.advogado.id = :id " +
                "AND p.encerrado = 'N'");
                query.setInteger("id", usr.getId().intValue());
            }else if(("Processos já encerrados").equalsIgnoreCase(criterio.getNome())){
                query = session.createQuery(
                "FROM Processo p " +
                "WHERE p.promovente.advogado.id = :id OR p.promovido.advogado.id = :id " +
                "AND p.encerrado = 'Y'");
                query.setInteger("id", usr.getId().intValue());
            }else if(("Processos em que foi promovido").equalsIgnoreCase(criterio.getNome())){
                query = session.createQuery(
                "FROM Processo p " +
                "WHERE p.promovido.advogado.id = :id");
                query.setInteger("id", usr.getId().intValue());
            }else if(("Processos em que foi promovente").equalsIgnoreCase(criterio.getNome())){
                query = session.createQuery(
                "FROM Processo p " +
                "WHERE p.promovente.advogado.id = :id");
                query.setInteger("id", usr.getId().intValue());
            }else if(("Processos que ganhou ou perdeu como promovido").equalsIgnoreCase(criterio.getNome())){
                query = session.createQuery(
                "FROM Processo p " +
                "WHERE p.promovido.advogado.id = :id " + 
                "AND p.encerrado = 'Y'");
                query.setInteger("id", usr.getId().intValue());
            }else if(("Processos que ganhou ou perdeu como promovente").equalsIgnoreCase(criterio.getNome())){
                query = session.createQuery(
                "FROM Processo p " +
                "WHERE p.promovente.advogado.id = :id " + 
                "AND p.encerrado = 'Y'");
                query.setInteger("id", usr.getId().intValue());
            }else{
                query = session.createQuery(
                "FROM Processo p " +
                "WHERE p.promovente.advogado.id = :id OR p.promovido.advogado.id = :id");
                query.setInteger("id", usr.getId().intValue());
            }
            
            List<Processo> list = query.list();
            
            session.close();
            
            return list;
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }
    
    public TipoFase getTipoFase(String tipoFase){
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            
            Query query = session.createQuery(
            "FROM TipoFase t " +
            "WHERE t.nome = :nome");
            query.setString("nome", tipoFase);
            
            TipoFase tipo = new TipoFase();
                    
            tipo = (TipoFase) query.uniqueResult();
       
            session.close();
            
            if(tipo != null){
                return tipo;
            }else{
                throw new RuntimeException("Erro");
            }
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    public void salvarFaseProcesso(FaseProcesso faseProcesso) {
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            
            session.beginTransaction();
            
            if(faseProcesso.getId() != null){
                session.update(faseProcesso);
            }else{
                session.save(faseProcesso);
            }
            
            session.getTransaction().commit();
            
            session.close();
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    public Processo buscarProcessoPorId(Long id) {
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            
            Criteria c = session.createCriteria(Processo.class);
            
            c.add(Restrictions.eq("id", id));
            
            Processo p = (Processo) c.uniqueResult();
            
            session.close();
            
            return p;
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    public List<RespostaJuiz> getRespostasJuiz() {
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            
            Criteria c = session.createCriteria(RespostaJuiz.class);
            
            List<RespostaJuiz> list = (List<RespostaJuiz>) c.list();
            
            session.close();
            
            return list;
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    public List<FaseProcesso> getFasesProcesso(Processo processoSelecionado) {
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            
            Criteria c = session.createCriteria(FaseProcesso.class);
            
            c.add(Restrictions.eq("processo", processoSelecionado));
            
            List<FaseProcesso> list = c.list();
            
            session.close();
            
            return list;
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    private Usuario getJuizComMenosProcessos() {
        try{
            Map<Integer, Usuario> qtdProcessos = new HashMap<>();
            UsuarioFacade f = new UsuarioFacade();
            for(Usuario obj: f.buscarJuizes()){
                Integer qtd = obj.getQuantidadeProcessos();
                qtdProcessos.put(qtd, obj);
            }
            
            return qtdProcessos.get(Collections.min(qtdProcessos.keySet()));
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    public List<CriterioConsulta> getCriteriosConsulta() {
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            
            Criteria c = session.createCriteria(CriterioConsulta.class);
            
            List<CriterioConsulta> list = c.list();
            
            session.close();
            
            return list;
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    public List<Usuario> getUsuariosPorAdvogado(Usuario advogado) {
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            
            Criteria c = session.createCriteria(Usuario.class)
                    .add(Restrictions.eq("advogado.id", advogado.getId()));
            
            List<Usuario> list = c.list();
            
            session.close();
            
            return list;
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }
}
