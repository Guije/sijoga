/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import model.Cidade;
import model.Estado;
import model.TipoUsuario;
import model.Usuario;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import util.HibernateUtil;

/**
 *
 * @author guilh
 */
public class UsuarioDAO {
    
    public Usuario login(String login, String senha){
        Session session = HibernateUtil.getSessionFactory().openSession();
      
        Criteria c = session.createCriteria(Usuario.class);
        
        c.add(Restrictions.eq("login", login))
                .add(Restrictions.eq("senha", senha));
            
        Usuario usr = (Usuario) c.uniqueResult();
        
        session.close();
        if(usr != null){
            return usr;
        }else{
            throw new RuntimeException("Usuario não encontrado");
        }
    }
    
    public List<Usuario> buscarJuizes(){
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            
            Query query = session.createQuery(       
            "FROM Usuario u " +
            "WHERE u.tipoUsuario.nome like :tipoUsuario");
            query.setString("tipoUsuario", "Juiz");
            List<Usuario> list = query.list();
            session.close();
            if(list != null){
                return list;
            }else{
                throw new RuntimeException("Erro");
            }
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }
    
    public List<Usuario> buscarPartes(){
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            
            Query query = session.createQuery(
            "FROM Usuario u " +
            "WHERE u.tipoUsuario.nome like :tipoUsuario");
            query.setString("tipoUsuario", "Parte");
            List<Usuario> list = query.list();
            session.close();
            if(list != null){
                return list;
            }else{
                throw new RuntimeException("Erro");
            }
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }
    
    public void deslogar(Usuario usuarioLogado){
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            
            usuarioLogado = null;
            
            session.close();
            HibernateUtil.getSessionFactory().close();
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    public List<Usuario> buscarAdvogados() {
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            
            Query query = session.createQuery(
            "FROM Usuario u " +
            "WHERE u.tipoUsuario.nome like :tipoUsuario");
            query.setString("tipoUsuario", "Advogado");
            
            List<Usuario> advogados = query.list();
            
            return advogados;
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    public List<Estado> getEstados() {
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
      
            Criteria c = session.createCriteria(Estado.class);
            
            List<Estado> estados = c.list();
            
            return estados;
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    public List<Cidade> getCidades() {
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
      
            Criteria c = session.createCriteria(Cidade.class);
            
            List<Cidade> cidades = c.list();
            
            return cidades;
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    public List<Cidade> getCidadesPorEstado(Estado estado) {
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
      
            Criteria c = session.createCriteria(Cidade.class);
            
            c.add(Restrictions.eq("estado", estado));
            
            List<Cidade> cidades = c.list();
            
            return cidades;
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    /*public List<TipoUsuario> getTiposUsuarios() {
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
      
            Query query = session.createQuery(
            "FROM TipoUsuario t " +
            "WHERE t.nome like :tipoUsuario");
            query.setString("tipoUsuario", "Parte");
            
            List<TipoUsuario> tipos = c.list();
            
            return tipos;
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }*/
}
